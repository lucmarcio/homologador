const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './*_test.js',
  output: './output',
  helpers: {
    WebDriver: {
        // coloque o endereço IP:porta da máquina que está rodando o servidor web
        url: 'http://localhost:8078/',
        browser: 'chrome',
        host: 'localhost',
        // coloque o IP do local onde está executando o Selenium
        port: 4444,
        restart: false,
        keepBrowserState: true,
        keepCookies: true,
        windowSize: '1920x1680',
        desiredCapabilities: {
            // unexpectedAlertBehaviour: 'dismiss',
            chromeOptions: {
                args: [ /*"--headless",*/ "--disable-gpu", "--no-sandbox" ]
            }
        }
    }
  },
  include: {
    I: './steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'homologador',
  translation: 'pt-BR',
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
    wdio: {
      enabled: true,
      services: ['selenium-standalone']
    }
  }
}
